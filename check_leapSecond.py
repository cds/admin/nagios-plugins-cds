#!/usr/bin/python3
import sys, subprocess, time, argparse, re
import os.path, datetime

#python plugin to Nagios probe for monitoring expiration of leap second file 'tcleaps.txt'

parser = argparse.ArgumentParser(description='practice parsing')
parser.add_argument('-w', action='store', dest='days_b4_warning', type=int, default=30, help='Number of days before the file expires that the plugin should throw a warning. Default is 30 days')
parser.add_argument('-c', action='store', dest='days_b4_critical', type=int, default=0, help='Number of days before the file expires that the plugin should throw a critical message. Default is 0 days.')
parser.add_argument('-f', action='store', dest='inputFile', default="/ligo/data/tcleaps/tcleaps.txt", help='Stores the input file. Default is /ligo/data/tcleaps/tcleaps.txt')

results=parser.parse_args()
#convert days_b4_warning and days_b4_critical to UTC time
if results.days_b4_warning < 0 or results.days_b4_critical < 0:
        sys.stdout.write ("UNKNOWN: c and w must be greater than or equal to 0")
        sys.exit(3)
if results.days_b4_warning < results.days_b4_critical:
        sys.stdout.write ("UNKNOWN: w must be greater than c")
        sys.exit(3)

UTC_warning = 86400*results.days_b4_warning
UTC_critical = 86400*results.days_b4_critical



if os.path.isfile(results.inputFile):

# IF leapseconds-listd file
        if (results.inputFile == "/export/ligo/data/tcleaps/leap-seconds.list"):
                in_file=open(results.inputFile, 'r')
                for line in in_file:
                        if re.match("(.*)expires on(.*)",line):
                                lineParts = line.split()
                                date_only_readable = lineParts[4] + " " + lineParts[5] + " " + lineParts[6]
                                #print exp_date_readable
                                exp_date_readable = datetime.datetime.strptime(date_only_readable, "%d %B %Y")
                                #print dt
                                exp_date = time.mktime(exp_date_readable.timetuple())
                                #print exp_date
        elif (results.inputFile == "/export/ligo/data/tcleaps/tcleaps.txt"): 
        # IF TCLEAPS File
                #get current expiration date in UTC from input file
                in_file=open(results.inputFile, 'r')
                file_lines=in_file.readlines()
                secondLine = file_lines[1].strip()  #gets the entire second line 
                lineParts = secondLine.split()
                #print lineParts[2]  
                exp_date = int(lineParts[2]) #3rd item in line 2
                #get human readable form of expiration date
                exp_date_readable= time.ctime(exp_date)
        else:
                sys.stdout.write ("UNKNOWN:" + results.inputFile + " is not a valid leap second file")
                sys.exit(3)




# Now check the time
        if time.time() < exp_date - UTC_warning and time.time() < exp_date-UTC_critical:
                sys.stdout.write("OK: Leap second file expires on: " + str(exp_date_readable))
                sys.exit(0)
        elif exp_date - UTC_warning <= time.time():
                sys.stdout.write("WARNING: Leap second file expires on: " + str(exp_date_readable))
                sys.exit(1)
        elif exp_date - UTC_critical <= time.time():
                sys.stdout.write("CRITICAL: Leap second file has expired.")
                sys.exit(2)
        else:
                sys.stdout.write ("UNKNOWN: Unknown if leap second file has expired")
                sys.exit(3) 

else:
        sys.stdout.write ("UNKNOWN: File cannot be found")
        sys.exit(3)
