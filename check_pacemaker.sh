#!/bin/bash

PCS="sudo /usr/sbin/pcs"
CRM_MON="sudo /usr/sbin/crm_mon"
GREP="/bin/grep"

PROGNAME=`/usr/bin/basename $0`
PROGPATH=`echo $0 | sed -e 's,[\\/][^\\/][^\\/]*$,,'`
REVISION="0.1"

. $PROGPATH/utils.sh

print_usage() {
    echo "Usage  : $PROGNAME [action]"
    echo "Actions:"
    echo "         maintenance: Checks if maintenance property is set to true"
    echo "         move       : Checks if there are manually moved resources"
    echo "         failed     : Checks if there are failed actions"
    echo "         inactive   : Checks if there are nodes offline"
    echo ""
    echo "Usage  : $PROGNAME --help"
    echo "Usage  : $PROGNAME --version"
}

print_help() {
    print_revision $PROGNAME $REVISION
    echo ""
    print_usage
    echo ""
    echo "pacemaker/corosync status reporter for nagios"
    echo ""
    support
}

check_standby() {
    if $CRM_MON -s | $GREP 'standby' > /dev/null; then
        statustxt=`$CRM_MON -s` 
	echo "WARNING: Node in standby.."$statustxt
	exit $STATE_WARNING
    else
	echo "OK: Standby Mode is inactive..."
	exit $STATE_OK
    fi
}

check_move() {
    if $PCS config | $GREP 'location cli-prefer' > /dev/null; then
	echo "WARNING: Manual move is active..."
	exit $STATE_WARNING
    else
	echo "OK: Manual move is inactive..."
	exit $STATE_OK
    fi
}

check_failed() {
    if $PCS status | awk '/Failed actions/ {seen = 1} seen {print}' | $GREP -v 'Failed actions:' > /dev/null; then
	echo "WARNING: Failed actions present..."
	exit $STATE_WARNING
    else
	echo "OK: No failed actions present..."
	exit $STATE_OK
    fi
}


check_offline() {
    if $CRM_MON -s | grep "offline" > /dev/null; then
        statustxt=`$CRM_MON -s` 
	echo "CRITICAL: Nodes offline..."$statustxt
	exit $STATE_CRITICAL
    else
	echo "OK: No nodes offline..."
	exit $STATE_OK
    fi
}

check_connection() {
    if ! $PCS config > /dev/null; then
	echo "CRITICAL: could not connect to PCS..."
	exit $STATE_CRITICAL
    fi
}

# Make sure the correct number of command line
# arguments have been supplied

if [ $# -lt 1 ]; then
    print_usage
    exit $STATE_UNKNOWN
fi

exitstatus=$STATE_UNKNOWN #default
while test -n "$1"; do
    case "$1" in
        --help)
            print_help
            exit $STATE_OK
            ;;
        -h)
            print_help
            exit $STATE_OK
            ;;
        --version)
            print_revision $PROGNAME $REVISION
            exit $STATE_OK
            ;;
        -V)
            print_revision $PROGNAME $REVISION
            exit $STATE_OK
            ;;
	maintenance)
	    check_connection
	    check_standby
	    ;;
	move)
	    check_connection
	    check_move
	    ;;
	failed)
	    check_connection
	    check_failed
	    ;;
	inactive)
	    check_connection
	    check_offline
	    ;;
        *)
            echo "Unknown argument: $1"
            print_usage
            exit $STATE_UNKNOWN
            ;;
    esac
    shift
done

exit $exitstatus
