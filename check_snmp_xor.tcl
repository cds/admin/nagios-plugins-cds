#!/usr/bin/tclsh

# Determine if this is an 'and', 'or', or 'xor'
regexp {.*check_snmp_(and|or|xor).*} $argv0 null oper

if {$oper != "and" && $oper != "or" && $oper != "xor" } {
    puts "Unknown operation $oper"
    exit 3
}

# Assume that check_snmp is in the same place as this script
regsub {(.*check_snmp)_(and|or|xor).*} $argv0 {\1} check_snmp

set firstConstraintPos [lsearch $argv "-o"]
if {$firstConstraintPos != -1} {
    set defaultArgs [lrange $argv 0 [expr $firstConstraintPos-1]]
} else {
    set defaultArgs $argv
}

set constraintList {}
while {$firstConstraintPos != -1} {
    lappend constraintList [lrange $argv $firstConstraintPos [expr $firstConstraintPos+3]]
    set argv [lreplace $argv $firstConstraintPos [expr $firstConstraintPos+3]]
    set firstConstraintPos [lsearch $argv "-o"]
}

set globalSuccess 0
set globalStatus {}
foreach el $constraintList {
#    puts "Running $check_snmp $defaultArgs $el"
    if {[catch {set probeStatus [eval exec $check_snmp $defaultArgs $el]} msg]} {
#        puts "set probeReturnCode [lindex $::errorCode 2]"
#        puts "probe message: $msg"
        set probeReturnCode [lindex $::errorCode 2]
#        if {$probeReturnCode > 0} {
#            incr globalSuccess
#        }
#        if {$globalSuccess != 0} {
#            set globalSuccess $probeReturnCode
#        }
        regsub {\|.*} $errorInfo {} probeStatus
#       puts $probeStatus
        set globalStatus "$globalStatus $probeStatus"
    } else {
        set probeReturnCode 0
        incr globalSuccess
#        set globalSuccess 0
#       puts $probeStatus
        regsub {\|.*} $probeStatus {} probeStatus
        set globalStatus "$globalStatus $probeStatus"
#        puts $globalStatus
#        exit $globalSuccess
    }
}

# TODO:  What should the status string show?
puts $globalStatus

switch $oper {
    and {
        if {$globalSuccess == [llength $constraintList]} {
            exit 0
        } else {
            exit 2
        }
    }
    or {
        if {$globalSuccess > 0} {
            exit 0
        } else {
            exit 2
        }
    }
    xor {
        if {$globalSuccess == 1} {
            exit 0
        } else {
            exit 2
        }
    }
}

puts "Unknown operation $oper"
exit 3
